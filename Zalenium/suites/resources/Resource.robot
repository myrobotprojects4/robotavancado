*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}      chrome

*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser   about:blank   ${BROWSER}   remote_url=http://192.168.0.16:4444/wd/hub

Fechar navegador
    Close Browser
