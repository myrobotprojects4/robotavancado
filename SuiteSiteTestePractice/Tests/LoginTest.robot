*** Settings ***
Resource            ../Resources/resource.robot
Resource            ../PageObject/HomePage.robot
Resource            ../PageObject/LoginPage.robot

Test Setup          Abrir navegador
Test Teardown       Fechar navegador

*** Test Case ***
Caso de Teste com PO 01: Adicionar Cliente
  Acessar a página home do site
  Clicar em "Sign in"
  Informar um e-mail válido
  Clicar em "Create an account"
  Preencher os dados obrigatórios
  Submeter cadastro
  Conferir se o cadastro foi efetuado com sucesso
