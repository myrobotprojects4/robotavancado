*** Settings ***
Library    SeleniumLibrary
Library    String

*** Variables ***
${HOME_URL}                 http://automationpractice.com
${HOME_TITLE}               My Store
${HOME_TOPMENU}             xpath=//*[@id="block_top_menu"]/ul
${LOGIN_LINK}               xpath=//*[@class='login']
${LOGIN_EMAIL}              id=email_create
${LOGIN_BTN_EMAIL}          id=SubmitCreate
${LOGIN_ESPERA_CAMPOS_USU}  xpath=//*[@id="account-creation_form"]//h3[contains(text(),"Your personal information")]
${LOGIN_RADIO_SX_MASC}      id=id_gender1
${LOGIN_NOME}               id=customer_firstname
${LOGIN_SOBRENOME}          id=customer_lastname
${LOGIN_SENHA}              id=passwd
${LOGIN_ENDERECO}           id=address1
${LOGIN_CIDADE}             id=city
${LOGIN_ESTADO}             id=id_state
${LOGIN_COD_POSTAL}         id=postcode
${LOGIN_CELULAR}            id=phone_mobile
${LOGIN_BNT_REGISTRAR}      id=submitAccount
${LOGIN_RESULTADO}          xpath=//*[@id="center_column"]/p
${LOGIN_NOME_REGISTRADO}    xpath=//*[@id="header"]/div[2]//div[1]/a/span

*** Keywords ***
#### Ações
Caso de Teste com PO 01: Adicionar Cliente
    Clicar em "Sign in"
    Informar um e-mail válido
    Clicar em "Create an account"
    Preencher os dados obrigatórios
    Submeter cadastro
    Conferir se o cadastro foi efetuado com sucesso

## Ações
Clicar em "Sign in"
   Click Element              ${LOGIN_LINK}

Informar um e-mail válido
    Wait Until Element Is Visible   ${LOGIN_EMAIL}
    ${EMAIL}                        Generate Random String
    Input Text                      ${LOGIN_EMAIL}    ${EMAIL}@testerobot.com

Clicar em "Create an account"
    Click Button    ${LOGIN_BTN_EMAIL}

Preencher os dados obrigatórios
    Wait Until Element Is Visible   ${LOGIN_ESPERA_CAMPOS_USU}
    Click Element                   ${LOGIN_RADIO_SX_MASC}
    Input Text                      ${LOGIN_NOME}                 Rafael
    Input Text                      ${LOGIN_SOBRENOME}            Teixeira
    Input Text                      ${LOGIN_SENHA}                123456
    Input Text                      ${LOGIN_ENDERECO}             Rua Framework, Bairro Robot
    Input Text                      ${LOGIN_CIDADE}               Floripa
    Set Focus To Element            ${LOGIN_ESTADO}
    ### No firefox ocorreu problema ao achar o listbox State, então coloquei um if para esperar
    ### pelo elemento quando for firefox
    Run Keyword If    '${BROWSER}'=='firefox'  Wait Until Element Is Visible   ${LOGIN_ESTADO}
    Select From List By Index       ${LOGIN_ESTADO}               9
    Input Text                      ${LOGIN_COD_POSTAL}           12345
    Input Text                      ${LOGIN_CELULAR}              99988877

Submeter cadastro
    Click Button                    ${LOGIN_BNT_REGISTRAR}

# Conferências
Conferir se o cadastro foi efetuado com sucesso
    Wait Until Element Is Visible    ${LOGIN_RESULTADO}
    Element Text Should Be           ${LOGIN_RESULTADO}
    ...    Welcome to your account. Here you can manage all of your personal information and orders.
    Element Text Should Be           ${LOGIN_NOME_REGISTRADO}    Rafael Teixeira
