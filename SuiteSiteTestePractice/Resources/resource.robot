*** Settings ***
Library         SeleniumLibrary
Documentation   Testo aqui

# Documentation   Para rodar os testes via headless, posso passar o comando logo abaixo (variavel BROWSER), a palavra
# ...             headlesschrome, ou via comando, da seguinte forma
# ...             robot -v Pasta1\Pasta2\arquivo.robot

*** Variables ***
${BROWSER}      chrome
### Opções necessárias para rodar headless no Linux do CI (runner)
${OPTIONS}      add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser   about:blank   ${BROWSER}   options=${OPTIONS}
    Maximize Browser Window

Fechar navegador
    Close Browser
